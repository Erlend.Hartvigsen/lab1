package rockPaperScissors;

// import java.util.Arrays;
// import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    String[] rpsChoices = {"rock" , "paper" , "scissors"};
    String playerMove;
    



    public void run() {
        
        while(true) {
            String computerMove = rpsChoices[new Random().nextInt(rpsChoices.length)]; // Gir computer en tilfeldig streng fra listen rpsChoices
            System.out.println("Let's play round " + roundCounter + "\nYour choice (Rock/Paper/Scissors)?");

            while(true) {
                
                playerMove = sc.nextLine();
                if (playerMove.equals("rock") || playerMove.equals("paper") || playerMove.equals("scissors"))
                    { 
                    break;
                }
                System.out.println("I do not understand " + playerMove + ". Could you try again?");
                System.out.println("Your choice (Rock/Paper/Scissors)?");
            }

            if (playerMove.equals(computerMove)) {
                System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". It's a tie!");
                roundCounter ++;
                System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            }

            else if (playerMove.equals("rock")) {
                if (computerMove.equals("paper")) {
                    System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". Computer wins!");
                    computerScore ++;
                    roundCounter ++;
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);

                } else if (computerMove.equals("scissors")) {
                    System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". Human wins!");
                    humanScore ++;
                    roundCounter ++;
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                }
            }

            else if (playerMove.equals("paper")) {
                if (computerMove.equals("scissors")) {
                    System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". Computer wins!");
                    computerScore ++;
                    roundCounter ++;
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);

                } else if (computerMove.equals("rock")) {
                    System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". Human wins!");
                    humanScore ++;
                    roundCounter ++;
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                }
            }

            else if (playerMove.equals("scissors")) {
                if (computerMove.equals("rock")) {
                    System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". Computer wins!");
                    computerScore ++;
                    roundCounter ++;
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);

                } else if (computerMove.equals("paper")) {
                    System.out.println("Human chose " + playerMove + ", computer chose " + computerMove + ". Human wins!");
                    humanScore ++;
                    roundCounter ++;
                    System.out.println("Score: human " + humanScore + ", computer " + computerScore);
                }
            }
            System.out.println("Do you wish to continue playing? (y/n)?");
            String playAgain = sc.nextLine();

            if (!playAgain.equals("y")) {
                System.out.println("Bye bye :)");
                break;
            }

            }

        
    }
 
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}